# Conda recipe for building VTK 7

## Instructions for use

1. Install miniconda
2. Update conda
  `conda update conda`
3. Install conda-build
  `conda install conda-build`
4. Update conda-build
  `conda update conda-build`
5. Clone this repo
  `git clone https://git.fmrib.ox.ac.uk/fsl/conda_vtk_recipe`
6. On Centos:
  `for i in conda_vtk_recipe; do yum -y install $i; done`
7. Make the jsoncpp project available:
  `conda config --add channels \
https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/channel/`
8. Ensure that zlib-devel is **not** installed (this causes cmake issues with 
using the conda provided zlib
  `yum remove zlib-devel`
9. Create build/test environment
  `conda create -n vtk_test python=3.5`
10. Build
  `conda build conda_vtk_recipe`
11. Upload the generated tar.bz2 file (path is revealed in the last few lines of
the build) to the web server (use the channel/osx-64 or channel/linux-64
as appropriate) and then run:
  `conda index channels/*`
on the web server in the parent.